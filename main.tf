# https://linuxconfig.org/configuring-virtual-network-interfaces-in-linux

terraform {
  required_providers {
    docker = {
      source = "kreuzwerker/docker"
    }
  }
}

# Declare variables

variable "supernode_count" {
  type        = number
  description = "Amount of supernodes"
  default     = 4

  validation {
    condition     = var.supernode_count >= 4
    error_message = "Supernode count must be greater than or equal to 4!"
  }
}

variable "node_count" {
  type        = number
  description = "Amount of peers (optional)"
  default     = 0

  validation {
    condition     = var.node_count >= 0
    error_message = "Node count cannot be less than zero!"
  }
}

# variable "vlan" {
#   type = object({
#     subnet  = string
#     gateway = string
#   })
#   description = "Docker VLAN information (CIDR and gateway)"
# }


# Build Docker images conditionally

resource "docker_image" "supernode" {
  name         = "supernode"
  force_remove = true

  build {
    path = "."
    tag  = ["supernode:latest"]
    build_arg = {
      type : "supernode"
    }
  }

  triggers = {
    dir_sha1 = sha1(join("", concat(
      [for f in fileset(path.module, "src/**/*.py") : filesha1(f)],
      [for f in fileset(path.module, "template/*") : filesha1(f)],
      [for f in fileset(path.module, "config/*") : filesha1(f)],
      [filesha1("Dockerfile"), filesha1("requirements.txt"), filesha1("main.py")]
    )))
  }
}

resource "docker_image" "node" {
  name         = "node"
  force_remove = true

  build {
    path = "."
    tag  = ["node:latest"]
    build_arg = {
      type : "node"
    }
  }

  triggers = {
    dir_sha1 = sha1(join("", concat(
      [for f in fileset(path.module, "src/**/*.py") : filesha1(f)],
      [for f in fileset(path.module, "template/*.j2") : filesha1(f)],
      [for f in fileset(path.module, "config/*.json") : filesha1(f)],
      [filesha1("Dockerfile"), filesha1("requirements.txt"), filesha1("main.py")]
    )))
  }
}


# Create the isolated network
resource "docker_network" "dht_network" {
  name       = "dht_network"
  driver     = "bridge"
  attachable = true
  ipv6       = false
}


# Start the containers

resource "docker_container" "supernodes" {
  count = var.supernode_count
  depends_on = [
    docker_image.supernode,
    # docker_network.dht_vlan
    docker_network.dht_network
  ]

  name       = "supernode-${count.index}"
  image      = "supernode:latest"
  tty        = true
  stdin_open = true
  # rm         = true
  start = true

  env = ["INDEX=${count.index}"]

  networks_advanced {
    name = "dht_network"
  }

  ports {
    internal = 6885
  }

  volumes {
    container_path = "/app/config/network"
    host_path      = abspath("${path.module}/config/network")
    read_only      = true
  }
}

resource "local_file" "supernode_addresses" {
  content = join(",", [
    for container in docker_container.supernodes.* :
    join("|", [container.name, join(":", [container.ip_address, container.ports[0].internal])])
  ])

  filename = "${path.module}/config/network/net.conf"
}

resource "docker_container" "nodes" {
  count = var.node_count
  depends_on = [
    docker_image.node,
    docker_network.dht_network
  ]

  name       = "node-${count.index}"
  image      = "node:latest"
  tty        = true
  stdin_open = true
  # rm         = true
  start = true

  networks_advanced {
    name = "dht_network"
  }

  volumes {
    container_path = "/app/config/network"
    host_path      = abspath("${path.module}/config/network")
    read_only      = true
  }
}
