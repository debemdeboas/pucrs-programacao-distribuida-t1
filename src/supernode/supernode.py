from ipaddress import IPv4Address
from multiprocessing import Queue
import socket
import threading
from typing import Tuple
from rich.console import Console
console = Console()

from ..dht.dht import DHT, DHTEntry, Peer
from ..util.message import Message, MessageType


class Supernode:
    def __init__(self, name: str, step: int, dht_min: int, dht_max: int, next_supernode_addr: Tuple[IPv4Address, int], total_supernodes_amount: int) -> None:
        self.name = name
        self.s: int = step
        self.next: Tuple[IPv4Address, int] = next_supernode_addr
        self.next_is_up = False
        self.amount_of_supernodes: int = total_supernodes_amount

        self.q: Queue[Message] = Queue()
        self.lock = threading.Lock()

        self.dht: DHT = DHT(dht_min, dht_max)

        self.clients: dict[str, Peer] = {}
        self.client_timers: dict[str, threading.Timer] = {}
        self.removed_clients = set()

        self.supernode_thread = threading.Thread(target=self.serve_forever, args=('0.0.0.0', 6884, ))
        self.supernode_thread.daemon = True
        self.supernode_thread.start()

        self.peer_thread = threading.Thread(target=self.serve_forever, args=('0.0.0.0', 6885, ))
        self.peer_thread.daemon = True

    def remove_client(self, ip: str):
        console.print('Removing client', ip)
        if ip in self.clients:
            self.clients.pop(ip)
        if ip in self.client_timers:
            self.client_timers.pop(ip)
        self.dht.remove_client_entries(ip)
        if ip not in self.removed_clients:
            self.send_to_next_supernode(Message(MessageType.REMOVE_CLIENT, ip))
        self.removed_clients.add(ip)

    def serve_forever(self, addr: str, port: int):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            sock.bind((addr, port))
            sock.listen()
            print('Listening on port', port)
            while True:
                client_sock, addr = sock.accept()
                client_addr = Peer.from_tuple(addr[0], addr[1])
                threading.Thread(target=self.handle_client, args=(client_sock, client_addr, ), daemon=True).start()

    def handle_client(self, sock: socket.socket, addr: Peer):
        data = sock.recv(4096)
        if not data:
            return
        msg = Message.deserialize(str(data, 'utf-8'))
        msg._from = (addr.ip.exploded, addr.port)
        try:
            match msg.type:
                case MessageType.ALIVE:
                    self.lock.acquire()
                    self.q.put(msg)
                    ret = self.q.get()
                    sock.sendall(ret.serialize_self())
                case MessageType.SUPERNODE_READY_Q:
                    response = Message.serialize(MessageType.SUPERNODE_READY_A)
                    sock.sendall(response)
                case MessageType.PEER_LOGIN:
                    self.lock.acquire()
                    self.q.put(msg)
                    response = Message.serialize(MessageType.PEER_LOGIN_OK, addr.ip.exploded)
                    sock.sendall(response)
                case MessageType.UPLOAD:
                    self.lock.acquire()
                    self.q.put(msg)
                    ret = self.q.get()
                    sock.sendall(ret.serialize_self())
                case MessageType.FIND:
                    self.lock.acquire()
                    self.q.put(msg)
                    ret = self.q.get()
                    sock.sendall(ret.serialize_self())
                case MessageType.LIST:
                    self.lock.acquire()
                    self.q.put(msg)
                    self.lock.release()
                    ret = self.q.get()
                    ret.type = MessageType.LIST
                    sock.sendall(ret.serialize_self())
                    ret.type = MessageType.LIST_FROM_SUPPEER
                    self.handle(ret)
                case MessageType.LIST_FROM_SUPPEER:
                    self.lock.acquire()
                    self.q.put(msg)
                    ret = self.q.get()
                    sock.sendall(ret.serialize_self())
                case MessageType.REMOVE_CLIENT:
                    self.lock.acquire()
                    self.q.put(msg)
                    sock.sendall(Message.serialize(MessageType.ACK))
                case _:
                    sock.sendall(msg.serialize_self())
        except:
            pass
        try:
            self.lock.release()
        except:
            pass

    def wait_for_next_node(self):
        print('Starting to wait')
        while True:
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
                sock.settimeout(20)
                target = (self.next[0].exploded, 6884)  #? Explicitly set port
                try:
                    sock.connect(target)
                    sock.sendall(Message.serialize(MessageType.SUPERNODE_READY_Q))
                    data = sock.recv(4096)
                    msg = Message.deserialize(str(data, 'utf-8'))
                    if msg.type == MessageType.SUPERNODE_READY_A:
                        self.handle(msg)
                        break
                    else:
                        print('Received wrong message type:', msg)
                except KeyboardInterrupt:
                    exit(1)
                except Exception as e:
                    console.print(e)
                    continue

    def start(self):
        while True:
            msg = self.q.get()
            try:
                self.handle(msg)
            except:
                pass

    def send_to_next_supernode(self, msg: Message) -> Message:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            sock.settimeout(30)
            target = (self.next[0].exploded, 6884)
            sock.connect(target)
            print('Sending', msg)
            sock.sendall(msg.serialize_self())
            while True:
                data = sock.recv(4096)
                if data:
                    break
        ret = Message.deserialize(data.decode('utf-8'))
        return ret

    def upload(self, msg: Message):
        content = msg.content
        entry = DHTEntry.from_string(content)

        if not entry.hash or not entry.name:
            console.print('Received empty message', msg)
            return Message(MessageType.ACK)

        if self.dht.validate_key(entry.hash):
            self.dht.add(entry)
            console.print(self.dht.to_table_self())
            return Message(MessageType.ACK)
        else: # Thank you, next!
            msg = Message(MessageType.UPLOAD, entry.to_string())
            return self.send_to_next_supernode(msg)

    def find(self, msg: Message):
        sender, findme = msg.content.split(maxsplit=1)
        if findme in self.dht:
            return Message(MessageType.FOUND, self.dht[findme].to_string())
        else: # Thank you, next!
            if sender == self.next[0].exploded:
                return Message(MessageType.NOT_FOUND)
            msg = Message(MessageType.FIND, msg.content)
            return self.send_to_next_supernode(msg)

    def list_all(self) -> Message:
        entries = self.dht.pack()
        msg = Message(MessageType.LIST_FROM_SUPPEER, self.name + '&&ENTRIES::' + entries)
        ret = self.send_to_next_supernode(msg)
        return ret

    def handle(self, msg: Message):
        print('Handling', msg)

        if not msg._from and msg.type != MessageType.SUPERNODE_READY_A:
            return

        match msg.type:
            case MessageType.SUPERNODE_READY_A:
                self.next_is_up = True
                self.peer_thread.start()
                print('Supernode ready!')
            case MessageType.PEER_LOGIN:
                client = Peer.from_tuple(*msg._from) # type:ignore
                self.clients[client.ip.exploded] = client
                self.client_timers[client.ip.exploded] = threading.Timer(5, self.remove_client, args=(client.ip.exploded, ))
                self.client_timers[client.ip.exploded].start()
            case MessageType.ALIVE:
                peer_ip = msg.content
                client = self.clients[peer_ip]
                self.client_timers[client.ip.exploded].cancel()
                self.client_timers[client.ip.exploded] = threading.Timer(5, self.remove_client, args=(client.ip.exploded, ))
                self.client_timers[client.ip.exploded].start()
                self.q.put(Message(MessageType.ACK))
            case MessageType.REMOVE_CLIENT:
                ip = msg.content
                self.remove_client(ip)
            case MessageType.UPLOAD:
                ret = self.upload(msg)
                self.q.put(ret)
            case MessageType.FIND:
                ret = self.find(msg)
                self.q.put(ret)
            case MessageType.LIST:
                self.list_all()
            case MessageType.LIST_FROM_SUPPEER:
                name = msg.content[:msg.content.index('&&ENTRIES::')]
                if name != self.name:
                    entries = self.dht.pack()
                    msg.content += entries
                    self.send_to_next_supernode(msg)
                self.q.put(Message(MessageType.ACK))
            case _:
                pass
