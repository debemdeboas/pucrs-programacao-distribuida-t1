
from genericpath import isdir, isfile
import os
from ipaddress import IPv4Address
import pathlib
import socket
import zipfile
from rich.prompt import Prompt, Confirm
import io
from typing import Any, Dict, List, Tuple
from ..util.hash import HashDirFunction, map_hash_to_hash_functions, HashFileFunction, HashDataFunction
import threading
from rich.console import Console
from rich.table import Table
import time
from ..dht.dht import DHT, DHTEntry, DataType, Peer
from ..util.message import Message, MessageType

console = Console()

class Node:
    def __init__(self, config: dict) -> None:
        self.hashing_algorithm = config['hashing_algorithm']
        hash_data, hash_file, hash_dir = map_hash_to_hash_functions(self.hashing_algorithm)
        self.hash_data: HashDataFunction = hash_data
        self.hash_file: HashFileFunction = hash_file
        self.hash_dir: HashDirFunction = hash_dir
        self.peer_listener = None
        self.peer_listener_ready = False
        self.superpeer: Peer
        self.us = Peer.from_tuple('0.0.0.0', -1)

        self.resources: Dict[str, DHTEntry] = dict()

        self.final_buffer: List[DHTEntry] = []
        self.aux_buffer: List[DHTEntry] = []

        self.commands = {
            'HELP': 'Prints help.',
            'CLS': 'Clears the screen (same as CLEAR).',
            'CLEAR': 'Clears the screen (same as CLS).',
            'WHOIS': 'Gets our IP and port.',
            'LS': 'Lists the files and folders in the current directory.',
            'BUFFER AUX': 'Prints the auxiliary buffer.',
            'BUFFER ADD': 'Adds the last hash to the final buffer.',
            'BUFFER RM': 'Clears the final buffer.',
            'BUFFER': 'Prints the state of the final buffer.',
            'HASH STRING': 'Hashes a string. Prints the hash and saves it to the auxiliary buffer.',
            'HASH FILE': 'Hashes a file. Prints the hash and saves it to the auxiliary buffer.',
            'HASH DIR': 'Hashes a directory (recursive). Prints the hash and saves it to the auxiliary buffer.',
            'PORT': 'Gets the peer listener socket port.',
            # 'LOGIN': 'Sends a login message to a supernode.',
            'LIST': 'Gets all DHT entries.',
            'FIND': 'Searches for a STRING in the DHT.',
            'UPLOAD': 'Uploads an entry to the DHT.',
            'DOWNLOAD': 'Downloads data from another peer.\n[blue]Usage:[white] DOWNLOAD <HASH> <IP:PORT>',
            'RESOURCES': 'Lists all of our downloaded/uploaded resources.',
            'DATA': 'Reads the DATA entry of a local resource.',
        }

        self.help_table = Table(title='Command reference')
        self.build_help_table()

        self.create_and_start_listener()
        self.create_and_start_alive_thread()

    def create_entry(self, _hash: str, name: str, type: DataType = DataType.str) -> DHTEntry:
        return DHTEntry(_hash, name, self.us, type)

    def create_and_start_listener(self):
        if self.peer_listener:
            console.print(f'Listening on port {self.server_port}')

        self.peer_listener = threading.Thread(target=self.serve_forever, args=('0.0.0.0', ))
        self.peer_listener.daemon = True
        self.peer_listener.start()
        while not self.peer_listener_ready:
            time.sleep(0.1)

    def create_and_start_alive_thread(self):
        def send_alive():
            while self.us.port == -1:
                time.sleep(0.1)

            while True:
                self.talk_to_superpeer(Message(MessageType.ALIVE, self.us.ip.exploded))
                time.sleep(1)

        alive_thread = threading.Thread(target=send_alive)
        alive_thread.daemon = True
        alive_thread.start()

    def login_approved(self, addr: str):
        self.us = Peer.from_tuple(addr, self.server_port)

    def serve_forever(self, addr: str):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            sock.bind((addr, 0))
            sock.listen()
            self.server_addr, self.server_port = sock.getsockname()
            self.peer_listener_ready = True
            console.print(f'Listening on port {self.server_port}')
            while True:
                client_sock, addr = sock.accept()
                threading.Thread(target=self.handle_client, args=(client_sock, addr, ), daemon=True).start()

    def handle_client(self, sock: socket.socket, addr):
        data = sock.recv(4096)
        if not data:
            return
        msg = Message.deserialize(str(data, 'utf-8'))
        msg._from = addr
        match msg.type:
            case MessageType.DOWNLOAD:
                if msg.content in self.resources:
                    downloaded_data = self.resources[msg.content]
                    response = Message.serialize(MessageType.DOWNLOAD_RESPONSE,
                                                 downloaded_data.to_string())
                    sock.sendall(response)
                else:
                    response = Message.serialize(MessageType.NOT_FOUND)
                    sock.sendall(response)
            case MessageType.DOWNLOAD_FILE:
                entry = DHTEntry.from_string(msg.content)
                f = open(os.path.join('shared', os.path.basename(entry.name)), 'rb')
                while True:
                    file_data = f.read(4096)
                    if not file_data:
                        # Transfer is done
                        console.print(f'\nFile transfer for {entry.name} is done.')
                        break
                    sock.sendall(file_data)
                sock.close()
            case MessageType.DOWNLOAD_DIR:
                entry = DHTEntry.from_string(msg.content)
                zip_buffer = io.BytesIO()
                with zipfile.ZipFile(zip_buffer, 'a', zipfile.ZIP_DEFLATED, False) as zipped:
                    for root, dirs, files in os.walk(os.path.join('shared', entry.name)):
                        for file in files:
                            zipped.write(os.path.join(root, file),
                                        os.path.relpath(os.path.join(root, file),
                                                        os.path.join('shared', entry.name, '..'))
                                        )
            case _:
                pass

    def download_from_peer(self, peer: Peer, _hash: str):
        msg = Message(MessageType.DOWNLOAD, _hash)

        target = (peer.ip.exploded, peer.port)
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            sock.settimeout(10)
            sock.connect(target)
            sock.sendall(msg.serialize_self())
            data = sock.recv(4096)

        ret = Message.deserialize(data.decode('utf-8'))
        match ret.type:
            case MessageType.DOWNLOAD_RESPONSE:
                entry = DHTEntry.from_string(ret.content)
                self.resources[entry.hash] = entry
            case MessageType.NOT_FOUND:
                console.print('Hash', _hash, 'not found on peer', peer)
                return
            case _:
                return

        match entry.type:
            case DataType.str:
                console.print('Downloaded string and added to downloaded resources. '\
                    'Run [bold]RESOURCES[regular] or [bold]DATA[regular] to read the data.')
            case DataType.file:
                if Confirm.ask(f'Do you want to download the file [underline]{entry.name}[regular]?', default=False):
                    self.download_file(target, entry)
                else:
                    self.resources.pop(entry.hash)
                    console.print(f'Removed file {entry.name} from our local resources.')
            case DataType.dir:
                if Confirm.ask(f'Do you want to download the directory [blue underline]{entry.name}[regular]?' + \
                    '\nIt will be downloaded as a ZIP file.', default=False):
                    self.download_dir(target, entry)
                else:
                    self.resources.pop(entry.hash)
                    console.print(f'Removed directory [blue underline]{entry.name}[regular] from our local resources.')


    def download_file(self, target: tuple[str, int], entry: DHTEntry):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(10)
        sock.connect(target)
        sock.sendall(Message.serialize(MessageType.DOWNLOAD_FILE, entry.to_string()))
        f = open(os.path.join('shared', os.path.basename(entry.name)), 'wb')
        data = sock.recv(4096)
        while data:
            f.write(data)
            data = sock.recv(4096)
            if not data:
                f.close()
                break
        sock.close()
        console.print(f'File [underline]{os.path.basename(entry.name)}[regular] downloaded.')


    def download_dir(self, target: tuple[str, int], entry: DHTEntry):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(10)
        sock.connect(target)
        sock.sendall(Message.serialize(MessageType.DOWNLOAD_DIR, entry.to_string()))
        f = open(os.path.join('shared', os.path.basename(entry.name)), 'wb')
        data = sock.recv(4096)
        while data:
            f.write(data)
            data = sock.recv(4096)
            if not data:
                f.close()
                break
        sock.close()
        console.print(f'Directory downloaded as ZIP file [underline]{os.path.basename(entry.name)}[regular].')


    def build_help_table(self):
        self.help_table.add_column('Command', justify='left', no_wrap=True, style='cyan')
        self.help_table.add_column('Description', justify='left')
        for cmd, desc in self.commands.items():
            self.help_table.add_row(cmd, desc)

    def validate(self, query: str) -> bool:
        return any(map(lambda cmd: query.upper().startswith(cmd), self.commands.keys()))

    def split_command(self, cmd: str) -> Tuple[str, str]:
        args = ''
        for command in self.commands.keys():
            if not cmd.upper().startswith(command):
                continue
            args = cmd[len(command):].strip()
            break
        else:
            raise Exception
        return command, args

    def configure_superpeer(self, ip, port, name: str) -> bool:
        self.superpeer = Peer(IPv4Address(ip), int(port))
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(5)
        try:
            sock.connect((ip, port))
        except:
            return False
        msg = Message(MessageType.PEER_LOGIN)
        self.talk_to_superpeer(msg)
        return True

    def talk_to_superpeer(self, msg: Message):
        if not self.superpeer:
            console.bell()
            console.print('[red]Superpeer not connected!')
            return

        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            target = (self.superpeer.ip.exploded, self.superpeer.port)
            #TODO add progress bar
            sock.settimeout(120)
            sock.connect(target)
            sock.sendall(msg.serialize_self())
            data, frm = sock.recvfrom(4096)

        try:
            msg = Message.deserialize(data.decode('utf-8'))
        except TypeError:
            console.print(f'Empty message from {frm}')

        match msg.type:
            case MessageType.ACK:
                pass
            case MessageType.PEER_LOGIN_OK:
                self.login_approved(msg.content)
            case MessageType.NOT_FOUND:
                return msg
            case MessageType.LIST:
                _, entries = msg.content.split('&&ENTRIES::', maxsplit=1)
                entries = entries.replace('++**DHTEntry**++\n++**DHTEntry**++',
                                          '++**DHTEntry**++').split('++**DHTEntry**++')
                entries = [DHTEntry.from_string(entry) for entry in filter(lambda e: bool(e), entries)]
                table = DHT.to_table('Distributed hash table', entries)
                console.print(table)
            case MessageType.FOUND:
                entry = DHTEntry.from_string(msg.content)
                table = DHT.to_table('Found match', [entry])
                console.print(table)
            case _:
                console.print('[red]Invalid message:[white]', msg)

    def start(self):
        input('PRESS [ENTER] TO CONTINUE')
        self.run()

    def interact(self):
        answr = Prompt.ask('CMD', default='HELP')

        if not self.validate(answr):
            console.print(f'[red]Invalid command![white] Type HELP to get help.')
            return

        try:
            cmd, args = self.split_command(answr)
        except:
            return

        match cmd:
            case 'HELP':
                console.print(self.help_table)
            case 'CLS' | 'CLEAR':
                console.clear()
            case 'WHOIS':
                console.print(self.us)
            case 'LS':
                folder = './shared'
                table = Table(title='Shared files and folders')
                table.add_column('Name', justify='center')
                table.add_column('Type', justify='left', style='white')
                if not os.path.exists(folder):
                    os.mkdir(folder)
                for f in os.listdir(folder):
                    if os.path.isfile(os.path.join(folder, f)):
                        table.add_row(f, 'File')
                    elif os.path.isdir(os.path.join(folder, f)):
                        table.add_row(f'[blue]{f}', 'Folder')
                console.print(table)
            case 'BUFFER AUX':
                console.print(self.aux_buffer)
            case 'BUFFER ADD':
                if args:
                    _hash, name = args.split(' ', maxsplit=1)
                    self.final_buffer.append(self.create_entry(_hash, name))
                    console.print(f'Added [green]{_hash} - {name} [white]to final buffer')
                elif self.aux_buffer:
                    self.final_buffer += self.aux_buffer
                    console.print(f'Added {self.aux_buffer} [white]to final buffer')
                    self.aux_buffer = list()
                else:
                    console.print('Auxiliary buffer is empty!')
            case 'BUFFER RM':
                self.final_buffer.clear()
                console.print('Buffer cleared')
            case 'BUFFER':
                console.print(self.final_buffer)
            case 'HASH STRING':
                hashee = args
                if not hashee:
                    hashee = Prompt.ask('String to hash')
                hashed = self.hash_data(hashee)
                console.print(hashed)
                self.aux_buffer.append(self.create_entry(hashed, hashee))
            case 'HASH FILE':
                if not args.startswith('shared/'):
                    args = './shared/' + args
                hashee = args
                if not pathlib.Path(hashee).is_file():
                    console.print('[red]File doesn\'t exist!')
                    return
                hashed = self.hash_file(hashee)
                console.print(hashed)
                self.aux_buffer.append(self.create_entry(hashed, hashee, DataType.file))
            case 'HASH DIR':
                if not args.startswith('shared/'):
                    args = './shared/' + args
                hashee = args
                if not pathlib.Path(hashee).is_dir():
                    console.print('[red]Directory doesn\'t exist!')
                    return
                hashed = self.hash_dir(hashee)
                console.print(hashed)
                self.aux_buffer.append(self.create_entry(hashed, hashee, DataType.dir))
            case 'PORT':
                console.print(f'Listening on {self.server_addr}:{self.server_port}')
            # case 'LOGIN':
            #     msg = Message(MessageType.PEER_LOGIN)
            #     self.talk_to_superpeer(msg)
            case 'LIST':
                self.talk_to_superpeer(Message(MessageType.LIST))
            case 'FIND':
                if not args:
                    console.print('[red]Not enough arguments!')
                    return
                msg = Message(MessageType.FIND, self.superpeer.ip.exploded + ' ' + args)
                ret = self.talk_to_superpeer(msg)
                console.print(ret)
            case 'UPLOAD':
                for entry in self.final_buffer:
                    self.resources[entry.hash] = entry
                    msg = Message(MessageType.UPLOAD, entry.to_string())
                    self.talk_to_superpeer(msg)
                self.final_buffer.clear()
            case 'DOWNLOAD':
                _hash, peer = args.split(' ', maxsplit=1)
                ip, port = peer.split(':')
                peer = Peer.from_tuple(ip, port)
                self.download_from_peer(peer, _hash)
            case 'RESOURCES':
                table = Table(title='Local resources')
                table.add_column('Hash', justify='center', style='cyan')
                table.add_column('Name', justify='center', style='white')
                for res in self.resources.values():
                    table.add_row(res.hash, res.name)
                console.print(table)
            case 'DATA':
                if args in self.resources:
                    console.print(self.resources[args])
                else:
                    console.print('[red]Not found!')

    def run(self):
        try:
            while True:
                self.interact()
        except KeyboardInterrupt:
            exit(0)
        except Exception as e:
            console.print_exception()
            self.run()
