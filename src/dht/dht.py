
from dataclasses import dataclass
from enum import Enum, auto
from ipaddress import IPv4Address
from typing import List
from rich.table import Table


class DataType(Enum):
    str = auto()
    file = auto()
    dir = auto()


@dataclass
class Peer:
    ip: IPv4Address
    port: int

    def __repr__(self) -> str:
        return self.ip.exploded + ':' + str(self.port)

    @staticmethod
    def from_tuple(ip: str, port: int | str) -> 'Peer':
        port = int(port)
        return Peer(IPv4Address(ip), port)


@dataclass
class DHTEntry:
    MSG_SEPARATOR = '||DHTField||'

    hash: str
    name: str
    peer: Peer
    type: DataType = DataType.str

    def to_string(self) -> str:
        return f'{self.hash}{DHTEntry.MSG_SEPARATOR}{self.name}{DHTEntry.MSG_SEPARATOR}{self.peer}{DHTEntry.MSG_SEPARATOR}{self.type.name}'

    @staticmethod
    def from_string(data: str) -> 'DHTEntry':
        items = data.split(DHTEntry.MSG_SEPARATOR)
        peer = items[2].split(':')
        peer = Peer.from_tuple(*peer)
        return DHTEntry(hash=items[0],
                        name=items[1],
                        peer=peer,
                        type=DataType[items[3]])


class DHT:
    def __init__(self, _min: int, _max: int) -> None:
        self.min = _min
        self.max = _max
        self.hash_table: dict[str, DHTEntry] = dict()

    def add(self, entry: DHTEntry):
        if not self.validate_key(entry.hash):
            raise IndexError
        self.hash_table[entry.hash] = entry

    def validate_key(self, key: str) -> bool:
        if self.min <= int(key, 16) <= self.max:
            return True
        return False

    def __contains__(self, item):
        return item in self.hash_table

    def __getitem__(self, k) -> DHTEntry:
        return self.hash_table[k]

    @staticmethod
    def to_table(title: str, entries: List[DHTEntry]) -> Table:
        table = Table(title=title)
        table.add_column('Hash', justify='center', style='cyan')
        table.add_column('Name', justify='center', style='white')
        table.add_column('Type', justify='center', style='white')
        table.add_column('Peer', justify='center', style='green')

        for entry in entries:
            table.add_row(entry.hash, entry.name, entry.type.name, f'{entry.peer.ip.exploded}:{entry.peer.port}')
        return table

    def to_table_self(self) -> Table:
        return self.to_table(f'DHT: {hex(self.min)} to {hex(self.max)}', list(self.hash_table.values()))

    def pack(self) -> str:
        return '++**DHTEntry**++'.join(entry.to_string() for entry in self.hash_table.values()) + '++**DHTEntry**++'

    def remove_client_entries(self, ip: str):
        to_be_removed = []
        for k, entry in self.hash_table.items():
            if entry.peer.ip == ip:
                to_be_removed.append(k)

        for k in to_be_removed:
            self.hash_table.pop(k)
