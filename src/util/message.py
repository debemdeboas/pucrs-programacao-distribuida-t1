from dataclasses import dataclass
from typing import Tuple
from enum import Enum, auto

MESSAGE_SEPARATOR = '*~~*.*~~*'

class MessageType(Enum):
    SUPERNODE_READY_Q = auto()
    SUPERNODE_READY_A = auto()
    PEER_LOGIN        = auto()
    PEER_LOGIN_OK     = auto()
    ACK               = auto()
    FAIL              = auto()
    UPLOAD            = auto()
    ALIVE             = auto()
    REMOVE_CLIENT     = auto()
    FIND              = auto()
    FOUND             = auto()
    NOT_FOUND         = auto()
    LIST              = auto()
    LIST_FROM_SUPPEER = auto()
    DOWNLOAD          = auto()
    DOWNLOAD_RESPONSE = auto()
    DOWNLOAD_FILE     = auto()
    DOWNLOAD_DIR      = auto()


@dataclass()
class Message:
    type: MessageType
    content: str = ''
    _from: Tuple[str, int] | None = None

    @staticmethod
    def deserialize(data: str) -> 'Message':
        if not data:
            raise TypeError(f'Empty message')
        type, content = data.split(MESSAGE_SEPARATOR)
        return Message(type=MessageType[type], content=content)

    @staticmethod
    def serialize(type: MessageType, content = '') -> bytes:
        return bytes(f'{type.name}{MESSAGE_SEPARATOR}{content}', 'utf-8')

    def serialize_self(self) -> bytes:
        return bytes(f'{self.type.name}{MESSAGE_SEPARATOR}{self.content}', 'utf-8')
