from typing import Protocol, Tuple
from functools import partial
import hashlib
import pathlib


class HashDataFunction(Protocol):
    def __call__(self, data: str) -> str: ...


class HashFileFunction(Protocol):
    def __call__(self, filename: str) -> str: ...


class HashDirFunction(Protocol):
    def __call__(self, dirname: str) -> str: ...


def from_file(hash_type, filename: str) -> str:
    _hash = hashlib.new(hash_type)
    with open(filename, 'rb') as f:
        for chunk in iter(lambda: f.read(4096), b''):
            _hash.update(chunk)
    return _hash.hexdigest()


def from_data(hash_type, data: str) -> str:
    _hash = hashlib.new(hash_type)
    _hash.update(data.encode('utf-8'))
    return _hash.hexdigest()


def from_dir(hash_type, dirname: str) -> str:
    _hash = hashlib.new(hash_type)
    from_dir_recur(_hash, dirname)
    return _hash.hexdigest()


def from_dir_recur(_hash, path):
    for file in sorted(pathlib.Path(path).iterdir(), key=lambda f: str(f).lower()):
        _hash.update(file.name.encode())
        if file.is_file():
            with open(file, 'rb') as f:
                for chunk in iter(lambda: f.read(4096), b''):
                    _hash.update(chunk)
        elif file.is_dir():
            from_dir_recur(_hash, file)


def map_hash_to_hash_functions(hash_type: str) -> Tuple[HashDataFunction, HashFileFunction, HashDirFunction]:
    match hash_type:
        case 'SHA-1':
            data = partial(from_data, 'sha1')
            file = partial(from_file, 'sha1')
            dir = partial(from_dir, 'sha1')
            return data, file, dir
        case 'SHA-2':
            data = partial(from_data, 'sha256')
            file = partial(from_file, 'sha256')
            dir = partial(from_dir, 'sha256')
            return data, file, dir
        case 'SHA-3':
            data = partial(from_data, 'sha3_256')
            file = partial(from_file, 'sha3_256')
            dir = partial(from_dir, 'sha3_256')
            return data, file, dir
        case 'MD5':
            data = partial(from_data, 'md5')
            file = partial(from_file, 'md5')
            dir = partial(from_dir, 'md5')
            return data, file, dir
    raise TypeError('Invalid hash name. Valid hashes are', hashlib.algorithms_available)
    # return lambda _: _, lambda _: _

