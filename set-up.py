
import jinja2
from rich.prompt import Prompt, IntPrompt
from rich.console import *

def render_config(algorithm, max_table_size, num_nodes):
    template_loader = jinja2.FileSystemLoader(searchpath='./template/')
    template_env = jinja2.Environment(loader=template_loader, autoescape=True)

    template = template_env.get_template('supernodes.json.j2')
    with open('config/supernodes.json', 'w') as f:
        f.write(template.render(dht_size=max_table_size, num_supernodes=num_nodes))

    template = template_env.get_template('nodes.json.j2')
    with open('config/nodes.json', 'w') as f:
        f.write(template.render(hashing_algorithm=algorithm))

def write_tfvars(num_supernodes, num_peers):
    with open('terraform.tfvars', 'w') as f:
        f.write(f'supernode_count = {num_supernodes}\n')
        f.write(f'node_count = {num_peers}\n')

if __name__ == '__main__':
    try:
        num_nodes = IntPrompt.ask('Number of supernodes')
        num_peers = IntPrompt.ask('Number of peers')
        hashing_algorithm = Prompt.ask('Desired hashing algorithm',
                                       choices=[
                                        'SHA-1', 'SHA-2', 'SHA-3', 'MD5',
                                       ], default='SHA-1')

        match hashing_algorithm:
            case 'SHA-1':  # 20 bytes, 40 hex chars
                max_table_size = 2 ** (8*20)
            case 'SHA-2':  # 32 bytes, 64 hex chars
                max_table_size = 2 ** (8*32)
            case 'SHA-3':  # 32 bytes, 64 hex chars
                max_table_size = 2 ** (8*32)
            case 'MD5':    # 16 bytes, 32 hex chars
                print('NOTE: MD5 is not secure and should not be used.')
                max_table_size = 2 ** (8*16)
            case _:
                print('Invalid algorithm')
                exit(1)

        render_config(hashing_algorithm, max_table_size, num_nodes)
        write_tfvars(num_nodes, num_peers)
    except Exception as e:
        console.log(e, style='bold red')
