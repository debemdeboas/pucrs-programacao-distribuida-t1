from ipaddress import IPv4Address
import random
import socket
from typing import Tuple
import typer
import json
import os
import time
import math
from rich.console import Console
console = Console()
from rich.progress import Progress, SpinnerColumn, TextColumn
from enum import Enum

from dotenv import load_dotenv
load_dotenv()

from src.supernode.supernode import Supernode
from src.node.node import Node

class NodeType(Enum):
    supernode = 'supernode'
    node = 'node'


app = typer.Typer()


def wait_for_supernode_configuration_files():
    with Progress(
        SpinnerColumn(),
        TextColumn('[progress.description]{task.description}')
    ) as progress:
        progress.add_task('Waiting for configuration files...')
        while True:
            if os.path.exists('config/supernodes.json') and \
                os.path.exists('config/network/net.conf'):
                break


def wait_for_node_configuration_files():
    with Progress(
        SpinnerColumn(),
        TextColumn('[progress.description]{task.description}')
    ) as progress:
        progress.add_task('Waiting for configuration files...')
        while True:
            if os.path.exists('config/nodes.json') and \
                os.path.exists('config/network/net.conf'):
                break


def read_configuration_file(filename: str) -> dict:
    with open(filename) as f:
        return json.load(f)


def read_network() -> dict:
    with open('config/network/net.conf') as f:
        lines = f.readlines()
    nodes = {}
    for line in lines:
        split = line.strip().split(',')
        for entry in split:
            name, addr = entry.split('|')
            ip, port = addr.split(':')
            node_index = name[name.index('-')+1:]
            nodes[node_index] = (ip, int(port), name)
    return nodes


def get_supernode_limits(conf) -> Tuple[int, int, int]:
    n = int(os.environ['INDEX'])
    N = conf['num_supernodes']
    x = conf['dht_size']
    s = math.floor(x/N)
    q_min = s * n
    if 0 <= n < N - 1:
        q_max = q_min + (s - 1)
    elif n == N - 1:
        q_max = x
    else:
        raise ValueError('Invalid DHT configuration')
    return s, q_min, q_max


def get_next_supernode(conf) -> Tuple[IPv4Address, int]:
    index = int(os.environ['INDEX'])
    next_index = index + 1
    if next_index == len(conf):
        next_index = 0
    return IPv4Address(conf[str(next_index)][0]), conf[str(next_index)][1]


def create_supernode():
    conf = read_configuration_file('config/supernodes.json')
    network = read_network()

    step, _min, _max = get_supernode_limits(conf)
    next_addr = get_next_supernode(network)

    return Supernode(network[os.environ['INDEX']][2], step, _min, _max, next_addr, conf['num_supernodes'])


def run_supernode():
    with Progress(
        SpinnerColumn(),
        TextColumn('[progress.description]{task.description}')
    ) as progress:
        progress.add_task('Creating supernode...')
        supernode = create_supernode()

    with Progress(
        SpinnerColumn(),
        TextColumn('[progress.description]{task.description}'),
    ) as progress:
        progress.add_task('Waiting for next node to come online...')
        supernode.wait_for_next_node()

    console.print('Starting supernode')
    supernode.start()


def run_node(skip_login: bool = False):
    with Progress(
        SpinnerColumn(),
        TextColumn('[progress.description]{task.description}')
    ) as progress:
        progress.add_task('Configuring...')
        conf = read_configuration_file('config/nodes.json')
        network = read_network()

        ip, port, name = random.choice(list(network.values()))

    _node = Node(conf)

    if skip_login:
        console.print('[red]Skipping login!')
    else:
        with Progress(
            SpinnerColumn(),
            TextColumn('[progress.description]{task.description}'),
        ) as progress:
            progress.add_task(f'Trying to log in to {ip}:{port}...')
            retries = 0
            while retries < 10:
                try:
                    inner_retries = 0
                    _node.configure_superpeer(ip, port, name)
                    while _node.us.port == -1 and inner_retries < 2:
                        time.sleep(0.5)
                        inner_retries += 1
                    if inner_retries >= 2:
                        log_in = False
                    else:
                        log_in = True
                        break
                except KeyboardInterrupt:
                    log_in = False
                    break
                except:
                    console.print_exception()
                retries += 1
                time.sleep(3)
            else:
                log_in = False

        if not log_in:
            console.print('[red]FATAL[white]\tCouldn\'t log in! Aborting.')
            exit(1)
    
    _node.start()


@app.command()
def run(type: NodeType = typer.Option(...)):
    match type:
        case NodeType.node:
            node(skip_login=False)
        case NodeType.supernode:
            supernode()


@app.command()
def supernode():
    wait_for_supernode_configuration_files()
    run_supernode()


@app.command()
def node(skip_login: bool = typer.Option(False, '--skip-login', '-s', help='Don\'t try to log into the network')):
    wait_for_node_configuration_files()
    run_node(skip_login)


if __name__ == '__main__':
    app()
