# PUCRS - Programação Distribuída - T1

[Este repositório](https://gitlab.com/debemdeboas/pucrs-programacao-distribuida-t1) contém todos os códigos, documentações, etc, referentes
ao trabalho 1 da cadeira de Programação Distribuída da PUCRS 2022/2.
Trabalho desenvolvido por Rafael Almeida de Bem e Vicente Vivian.

Esse trabalho consiste em implementar um sistema P2P (*peer-to-peer*)
distribuído de arquitetura híbrida controlado por um grupo de pelo menos quatro supernodos.
É utilizado o mesmo comando inicial tanto para supernodos quanto para nodos comuns - o tipo é passado via argumento, assim como arquivos de configuração.

## Terminal do nodo

Cada nodo faz login automaticamente na rede distribuída e espera um input do usuário (<kbd>ENTER</kbd>)
para continuar. Para ver os comandos disponíveis podemos digitar `HELP`.

Podemos "hashear" strings, arquivos e diretórios utilizando os seguintes comandos, respectivamente:

- `HASH STRING`
- `HASH FILE`
- `HASH DIR`

Os resultados serão adicionados a um buffer temporário que pode ser
visualizado com o comando `BUFFER AUX`.

Para enviar nossas hashes para os supernodos devemos rodar os seguintes comandos:

```
> BUFFER ADD
> UPLOAD
```

O comando `BUFFER ADD` adiciona o buffer auxiliar ao buffer definitivo.
Apenas os dados no buffer definitivo serão enviados.

Podemos rodar o comando `LIST` para listarmos a DHT.

## Subindo o sistema

Para subir o sistema são necessárias as seguintes tecnologias:

- [Docker](https://docs.docker.com/get-docker/)
- [Terraform](https://www.terraform.io/downloads)
- Python 3.10 com os pacotes necessários

Após a instalação devemos rodar os seguintes comandos:

```shell
$ python set-up.py
# Interaja com o programa para configurar o ambiente
$ terraform apply -auto-approve
```

Para conectar-se a um nodo podemos rodar o comando `docker attach <nodo desejado>`.
Por exemplo, para conectarmos ao nodo 0:

```shell
$ docker attach node-0
```

Para sair de um nodo devemos pressionar <kbd>Ctrl</kbd>+<kbd>C</kbd>.
O container irá parar, mas ele pode ser iniciado novamente usando o comando `docker start <nodo desejado>`.

## Arquivos e diretórios compartilhados

O sistema suporta hashear arquivos e diretórios para adicioná-los
à DHT. Entretanto, esses arquivos devem estar na pasta `./shared` de algum nodo (container).

### Adicionando arquivos

Para adicionar um arquivo a um container podemos rodar o seguinte comando:

```shell
$ docker cp <src> <container>:<dest>
```

Por exemplo, para adicionarmos o arquivo `FooBar.jpg` ao nodo 1:

```shell
$ docker cp FooBar.jpg node-0:/app/shared/
```

### Adicionando diretórios

Para adicionar um diretório ao container, o fluxo é praticamente igual.
Por exemplo, para adicionarmos o diretório `config/` ao nodo 6:

```shell
$ docker cp config node-6:/app/shared/
```

Podemos verificar que as cópias funcionaram podemos rodar o comando `LS` nos nodos que possuem os arquivos.

<!-- https://rich.readthedocs.io/en/stable/live.html -->
