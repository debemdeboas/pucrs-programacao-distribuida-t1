ARG type

# Supernode conditional build
FROM python:3.10-alpine as build_supernode
ONBUILD RUN echo "Building supernode"
ONBUILD COPY config/supernodes.json /app/config/supernodes.json
ONBUILD EXPOSE 6884 6885


# Node (peer) conditional build
FROM python:3.10-alpine as build_node
# Don't copy network configuration file because we don't know if it exists (yet)
# ONBUILD COPY .noop config/network/net.conf /app/config/network/net.conf
ONBUILD RUN echo "Building node"
ONBUILD COPY config/nodes.json /app/config/nodes.json
ONBUILD RUN mkdir -p /app/shared


# Build the generic image
FROM build_${type}
ARG type

RUN apk add tcpdump

WORKDIR /app

COPY requirements.txt /app/requirements.txt
RUN pip install --no-cache-dir --upgrade -r /app/requirements.txt

COPY main.py main.py
COPY src /app/src

ENV TYPE=${type}

ENTRYPOINT python main.py run --type $TYPE
